import React from 'react'

export default function Footer() {
  return (
    <footer>
        All rights reserved &copy;
        <p>Vinnytsia 2023</p>
    </footer>
  )
}
