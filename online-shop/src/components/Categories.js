import React, { Component } from 'react'

class Categories extends Component {
    constructor(props) {
      super(props);
      this.state = {
        categories: []
      };
    }
  
    componentDidMount() {
      fetch('https://api.rawg.io/api/genres?key=d7126b33a3ad4632ac0f812f712ccb4c')
        .then(response => response.json())
        .then(data => {
          if (data.results && data.results.length) {
            const categories = data.results.map(({ slug, name }) => ({
              key: slug,
              name
            }));
            this.setState({
              categories: [
                {
                  key: 'all',
                  name: 'All'
                },
                ...categories
              ]
            });
          }
        })
        .catch(error => {
          console.error('Error fetching category data:', error);
        });
    }
  
    render() {
      return (
        <div className="categories">
          {this.state.categories.map(el => (
            <div key={el.key} onClick={() => this.props.chooseCategory(el.key)}>
              {el.name}
            </div>
          ))}
        </div>
      );
    }
  }

export default Categories