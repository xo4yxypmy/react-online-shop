import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Items from "./components/Items";
import Categories from "./components/Categories";
import ShowFullItem from "./components/ShowFullItem";

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      orders: [],
      currentItems: [],
      items:[],
      prices:[
        {
          id: 3498,
          price: '49.99'
        },
        {
          id: 3328,
          price: '29.99'
        },
        {
          id: 4200,
          price: '9.99'
        },
        {
          id: 5286,
          price: '12.99'
        },
        {
          id: 4291,
          price: '00.00'
        },
        {
          id: 13536,
          price: '4.99'
        },
        {
          id: 12020,
          price: '8.50'
        },
        {
          id: 5679,
          price: '15.30'
        },
        {
          id: 4062,
          price: '13.45'
        },
        {
          id: 28,
          price: '35.99'
        },
        {
          id: 3439,
          price: '7.99'
        },
        {
          id: 802,
          price: '14.99'
        },
        {
          id: 13537,
          price: '6.99'
        },
        {
          id: 4286,
          price: '10.99'
        },
        {
          id: 1030,
          price: '4.99'
        },
        {
          id: 58175,
          price: '49.99'
        },
        {
          id: 32,
          price: '00.00'
        },
        {
          id: 3070,
          price: '29.99'
        },
        {
          id: 2454,
          price: '19.99'
        },
        {
          id: 11859,
          price: '00.00'
        }
      ],
      showFullItem: false,
      fullItem: {}
    }
    this.state.currentItems = this.state.items
    this.addToOrder = this.addToOrder.bind(this)
    this.deleteOrder = this.deleteOrder.bind(this)
    this.chooseCategory = this.chooseCategory.bind(this)
    this.onShowItem = this.onShowItem.bind(this)
  }

  componentDidMount() {
    fetch('https://api.rawg.io/api/games?key=d7126b33a3ad4632ac0f812f712ccb4c')
      .then(response => response.json())
      .then(data => {
        if (data.results && data.results.length) {
          const items = data.results.map(({ id, name, background_image, genres }) => {
            const priceObject = this.state.prices.find(price => price.id === id);
            const price = priceObject ? priceObject.price : '';
  
            return {
              id,
              title: name,
              img: background_image,
              category: genres.map(({ slug }) => slug),
              price
            };
          });
  
          this.setState({
            items,
            currentItems: items
          });
        }
      })
      .catch(error => {
        console.error('Error fetching game data:', error);
      });
  }
  

  render(){
    return (
      <div className="wrapper">
        <Header orders={this.state.orders} onDelete={this.deleteOrder} />
        <Categories chooseCategory={this.chooseCategory} />
        <Items onShowItem={this.onShowItem} items={this.state.currentItems} onAdd={this.addToOrder} />
        
        {this.state.showFullItem && <ShowFullItem onAdd={this.addToOrder} onShowItem={this.onShowItem} item={this.state.fullItem} />}
        <Footer />  
      </div>
    );
  }

  onShowItem(item){
    this.setState({
      fullItem: item, 
      showFullItem: !this.state.showFullItem
    });
  }

  chooseCategory(category){
    if(category === 'all'){
      this.setState({currentItems: this.state.items})
      return
    }
    this.setState({
      currentItems: this.state.items.filter(el => el.category.includes(category))
    });
  }

  deleteOrder(id){
    this.setState({orders: this.state.orders.filter(el => el.id !== id)})
  }

  addToOrder(item){
    let isInArray = false
    this.state.orders.forEach(el => {
      if(el.id === item.id)
        isInArray = true
    })
    if(!isInArray)
      this.setState({orders: [...this.state.orders, item] })
  }

}

export default App;
